/*
 * gulp-apexify
 * https://bitbucket.org/sleelin/gulp-apexify
 *
 * Copyright (c) 2015 S. Lee-Lindsay
 * Licensed under the GNU license.
 */

"use strict";

var path = require("path"),
    through = require("through2"),
    Apexify = require("apexify"),
    match = require("minimatch"),
    _ = require("lodash"),
    gutil = require("gulp-util"),
    File = gutil.File,
    PluginError = gutil.PluginError,
    apexifier = new Apexify(),
    PLUGIN_NAME = "gulp-apexify";

module.exports = function (resources) {
    if (_.isUndefined(resources) || !(typeof resources.on === "function" && typeof resources.pipe === "function")) {
        throw new PluginError(PLUGIN_NAME, "Resources must be specified as a vinyl file stream!");
    } else {
        return through.obj(function transform(file, enc, next) {
            var stream = this;

            if (file.isStream()) {
                stream.emit("error", new PluginError(PLUGIN_NAME, "Streaming is not supported"));
                return next();
            }

            if (file.isBuffer()) {
                apexifier.processPage(file.contents.toString()).then(function (page) {
                    file.path = [path.dirname(file.path), "page"].join(".");
                    file.contents = new Buffer(page);
                    stream.push(file);
                });
            }

            next();
        }, function flush(next) {
            var stream = this;

            resources.pipe(through.obj(function (file, enc, next) {
                apexifier.addResource(file.relative, file.contents);
                next();
            })).on("finish", function () {
                apexifier.resolveDependencies(function (res) {
                    stream.push(new File({
                        path: ["staticresources", res.dest].join("/"),
                        contents: res.get()
                    }));
                }).finally(function () {
                    next(null, null);
                });
            });
        });
    }
};

module.exports.dependencies = function (filepath, required) {
    var bundle = apexifier.addBundle(filepath);

    return through.obj(function (file, enc, next) {
        if (file.isBuffer()) {
            bundle.consider(file.relative, file.contents, required.some(_.partial(match, file.relative)));
        }

        next();
    });
};